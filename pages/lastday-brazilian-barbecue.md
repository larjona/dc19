---
name: Brazilian Barbecue
---
# Brazilian Barbecue

The barbecue will take place at [APUFPR](https://salsa.debian.org/debconf-team/public/share/debconf19/tree/master/photos/organization/apufpr).

We will have options for vegans/vegetarians.

2 buses will take people to the barbecue leaving Hotel Nacional Inn at 12h.

Menu:
* Meet
* Chicken
* Sausage
* Rice
* Corn cream
* Cheese
* Eggplant
* Zucchini
* Lettuce
* Tomato
* Arugula
* Onion
* Palmito

APUFPR address: Rua Dr Alcides Vieira Arcoverde, 1193 - Jardim das Américas
