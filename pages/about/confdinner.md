---
name: Conference Dinner
---
# Conference Dinner

Every year, we celebrate DebConf with a special dinner.

## When?

 * July 25th, 2019 (Thursday).
 * 19:00h

## Where?

 * <strike>Le Dani Restaurante</strike>
 * <strike>Address: Rua Eugênio José de Souza, 1380 - Água Verde, Curitiba - PR.</strike>

 * Clube Curitibano Sede Concórdia
 * Rua Presidente Carlos Cavalcanti, 815 - Centro - Curitiba - PR

## Transportation by bus

Departure from Nacional Inn Hotel from 6:30 pm to 7:15 pm

Return from Clube Curitibano at:

- 10:00 pm
- 11:00 pm
- 11:30 pm
- 00:00 pm

## Cost

If you have a food bursary, the conference dinner is included.

If you are self-paying for food, the conference dinner will cost $20 USD
(R$ 75,00).

## Registration

Select the conference dinner during registration.
If appropriate, the cost will be added to your bill.
Pay online, or at the front-desk.

## Food

To be defined.

## Drinks

To be defined.

### Beer

To be defined.

### Wine

To be defined.

### Non-alcoholic

 * Water, sparkling water
 * Soda: Coke and Guaraná
 * Orange Juice

