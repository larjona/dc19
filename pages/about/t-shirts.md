---
name: T-Shirt Sizes
---

# T-Shirt sizes

<img class="img-fluid" style="max-height: 20rem" src="{% static "img/tshirt-size.jpg" %}">

## Straight Cut:

| Brazilian Size | Std. Size | A (Height) | B (Width) |
|----------------|-----------|------------|-----------|
| PP             | XS        | 66cm       | 48cm      |
| P              | S         | 68cm       | 50cm      |
| M              | M         | 70cm       | 52cm      |
| G              |           | 71cm       | 54cm      |
| GG             | L         | 74cm       | 57cm      |
| XG             | XL        | 79cm       | 60cm      |
| XXG            | XXL       | 81cm       | 67cm      |
| XXXG           | XXXL      | 84cm       | 72cm      |

## Woman's Fitted Cut:

| Brazilian Size | Std. Size | A (Height) | B (Width) |
|----------------|-----------|------------|-----------|
| P              | S         | 61cm       | 42cm      |
| M              | M         | 64cm       | 44cm      |
| G              | L         | 66cm       | 47cm      |
| GG             | XL        | 68cm       | 50cm      |

## Children's sizes:

| Size | A (Height) | B (Width) |
|------|------------|-----------|
| 1    | 38cm       | 28cm      |
| 2    | 46cm       | 33cm      |
| 4    | 49cm       | 36cm      |
| 6    | 49cm       | 38cm      |
| 8    | 52cm       | 38cm      |
| 10   | 57cm       | 41cm      |
| 12   | 64cm       | 43cm      |
| 14   | 64cm       | 46cm      |
| 16   | 66cm       | 48cm      |
