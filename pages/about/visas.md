---
name: Visas
---
# Visas

Brazil operates its borders on a reciprocity policy, so if your country requires
a visa from Brazilian citizens, most likely Brazil will require a visa from you.

* South American citizens do not need a visa;
* European citizens from countries in the Schengen area do not need a visa;
* [(new)](http://www.in.gov.br/materia/-/asset_publisher/Kujrw0TZC2Mb/content/id/67423098) US, Canadian, Japan and Australian citizens will not need a visa after June 17th; 
* Other countries most likely require a visa.

Please check the [official listing of visa requirements for entering Brazil][visa-reqs].
Wikipedia also has [a page on the subject][wp-visas], which you may find
useful.

[visa-reqs]: http://www.portalconsular.itamaraty.gov.br/images/qgrv/QGRVsimples.ing.11.12.pdf
[wp-visas]: https://en.wikipedia.org/wiki/Visa_policy_of_Brazil

If you need any help obtaining a visa, please email the visa team
<[visa@debconf.org][]> with your details.

[visa@debconf.org]: mailto:visa@debconf.org

Please bring a printout of your registration confirmation email, for
immigration officers to examine.

## CA Certificates

The Brazilian government uses its own CA PKI, which is not trusted by
Mozilla or Debian. So you're very likely to run into certificate errors
when requesting a visa, online.

You can find the root certificates [here](http://www.iti.gov.br/navegadores/mozilla-firefox).

| Certificate | SHA-256 Fingerprint |
|-------------|---------------------|
| [Autoridade Certificadora Raiz Brasileira v1](http://acraiz.icpbrasil.gov.br/repositorio/v1_ff.der) | `CB:D8:ED:38:D4:A2:D6:77:D4:53:D7:0D:D8:89:0A:F4:` `F6:37:4C:BA:62:99:94:3F:1A:B3:A6:93:6C:6F:D7:95` |
| [Autoridade Certificadora Raiz Brasileira v2](http://acraiz.icpbrasil.gov.br/repositorio/v2_ff.der) | `FB:47:D9:2A:99:09:FD:4F:A9:BE:C0:27:37:54:3E:1F:` `35:14:CE:D7:47:40:7A:8D:9C:FA:39:7B:09:15:06:7C` |
| [Autoridade Certificadora Raiz Brasileira v5](http://acraiz.icpbrasil.gov.br/repositorio/v5_ff.der) | `CA:A5:3F:C6:09:1C:69:51:88:7C:97:6E:37:8F:6E:F8:` `9A:A6:37:7C:55:D9:7B:64:75:42:2B:71:ED:7E:9B:17` |
| [Autoridade Certificadora Raiz Brasileira v8](http://acraiz.icpbrasil.gov.br/repositorio/v8_ff.der) | `CD:54:06:F6:1A:8D:5D:08:A3:79:7D:A0:44:BE:66:9C:` `55:5C:2D:8B:82:1D:67:57:7F:0A:2C:E3:A1:A2:77:E1` |
| [Autoridade Certificadora Raiz Brasileira v9](http://acraiz.icpbrasil.gov.br/repositorio/v9_ff.der) | `CD:D1:14:9D:CF:C1:37:C4:C3:4F:A8:44:CF:59:2C:FB:` `FE:A0:5C:B1:3B:77:0A:D5:E1:F0:C7:FC:8C:9C:43:BF` |
