---
title: Lenovo Platinum Sponsor of DebConf19
---

Leia em Português abaixo.

We are very pleased to announce that [**Lenovo**][lenovo]
has committed to supporting [DebConf19][debconf19] as a **Platinum sponsor**.

*"Lenovo is proud to sponsor the 20th Annual Debian Conference."*
said Egbert Gracias, Senior Software Development Manager at Lenovo. *"We’re excited to see,
up close, the great work being done in the community and to meet the developers
and volunteers that keep the Debian Project moving forward!”*

Lenovo is a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as AR/VR
devices, smart home/office solutions and data center solutions.

With this commitment as Platinum Sponsor,
Lenovo is contributing to make possible our annual conference,
and directly supporting the progress of Debian and Free Software,
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Lenovo, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org][sponsors], and
visit the DebConf19 website at [https://debconf19.debconf.org][debconf19].

This news item was originally posted in [the Debian blog][blog]

*****

# Lenovo Patrocinador Platinum da DebConf19

Estamos muito felizes em anunciar que
a [**Lenovo**][lenovo] comprometeu-se a apoiar
a [DebConf19][debconf19] como um **patrocinador
Platinum**.

*"A Lenovo está orgulhosa de apoiar a 20ª Conferência Anual do
Debian."*, disse Egbert Gracias, Gerente Sênior de Desenvolvimento de
Software da Lenovo. *"Nós estamos animados para ver de perto o ótimo
trabalho que está sendo feito pela comunidade, e para conhecermos os
desenvolvedores e voluntários que mantêm o Projeto Debian seguindo em
frente!"*

A Lenovo é uma líder tecnológica global, e fabrica uma gama variada de
produtos, incluindo *smartphones*, *tablets*, *PCs* e *workstations*,
bem como dispositivos de realidade aumentada/virtual, soluções para
casas/escritórios inteligentes e para *datacenters*.

Com esse comprometimento como Patrocinador Platinum, a Lenovo
contribui para tornar possível nossa conferência anual e apoia
diretamente o progresso do Debian e do Software Livre, ajudando a
fortalecer a comunidade, que continua a colaborar com projetos do
Debian durante o restante do ano.

Muito obrigado Lenovo por apoiar a DebConf19!

## Torne-se um patrocinador também!

A DebConf19 ainda está aceitando patrocinadores. Empresas e
organizações interessadas devem entrar em contato com o time da
DebConf através
do [sponsors@debconf.org][sponsors], e visitar o
website da DebConf19
em [https://debconf19.debconf.org][debconf19].


Esta notícia foi originalmente publicada no [blog do Debian][blogpt].


[debconf19]: https://debconf19.debconf.org
[debconf]: https://www.debconf.org
[lenovo]: https://www.lenovo.com/
[sponsors]: mailto:sponsors@debconf.org
[blog]: https://bits.debian.org/2019/05/lenovo-platinum-debconf19.html
[blogpt]: https://bits.debian.org/2019/05/lenovo-platinum-debconf19-pt-BR.html
