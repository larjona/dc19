---
title: Google Platinum Sponsor of DebConf19
---

Leia em Português abaixo.

We are very pleased to announce that [**Google**][google]
has committed to support [DebConf19][debconf19] as a **Platinum sponsor**.

*"The annual DebConf is an important part of the Debian development ecosystem
and Google is delighted to return as a sponsor in support of the work
of the global community of volunteers who make Debian and DebConf a reality"*
said Cat Allman, Program Manager in the Open Source Programs
and Making & Science teams at Google.

Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products
as online advertising technologies, search, cloud computing, software, and hardware.

Google has been supporting Debian by sponsoring DebConf since more than
ten years, and is also a Debian partner sponsoring parts 
of [Salsa][salsa]'s continuous integration infrastructure
within Google Cloud Platform.


With this additional commitment as Platinum Sponsor for DebConf19,
Google contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Google, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org][sponsors], and
visit the DebConf19 website at [https://debconf19.debconf.org][debconf19].

This news item was originally posted in [the Debian blog][blog].

*****

# Google Patrocinador Platinum da DebConf19

Estamos muito felizes em anunciar que
o [**Google**][google]  comprometeu-se a apoiar
a [DebConf19][debconf19] como um **patrocinador
Platinum**.

*"A DebConf é uma parte importante do ecosistema do desenvolvimento do Debian
e o Google tem o prazer de retornar como um patrocinador em apoio ao trabalho
da comunidade global de voluntários que fazem do Debian e da DebConf uma
realidade"* disse Cat Allman, Gerente do Programa de Código Aberto e equipe de
Criação & Ciência do Google.

O Google é uma das maiores empresas de tecnologia do mundo, fornecendo uma
ampla gama de serviços e produtos relacionados à internet como tecnologias de
publicidade on-line, pesquisa, computação em nuvem, software e hardware.

O Google tem apoiado o Debian através do patrocínio da DebConf a mais de dez
anos, e é também um parceiro Debian que patrocina parte da infraestrutura de
integração contínua do [Salsa][salsa] na plataforma em nuvem da Google
(Google Cloud).

Com este compromisso adicional como Patrocinador Platinum da DebConf19, o
Google contribue para tornar possível a nossa Conferência anual, e apoia
diretamente o progresso do Debian e do Software Livre ajudando a fortalecer
a comunidade que continua a colaborar nos projetos Debian durante o resto do ano.

Muito obrigado ao Google por apoiar a DebConf19!

## Torne-se um patrocinador também!

A DebConf19 ainda está aceitando patrocinadores. Empresas e
organizações interessadas devem entrar em contato com o time da
DebConf através
do [sponsors@debconf.org][sponsors], e visitar o
website da DebConf19
em [https://debconf19.debconf.org][debconf19].

Esta notícia foi originalmente publicada no [blog do Debian][blogpt]

[debconf19]: https://debconf19.debconf.org
[debconf]: https://www.debconf.org
[google]: https://www.google.com
[salsa]: https://salsa.debian.org
[sponsors]: mailto:sponsors@debconf.org
[blog]: https://bits.debian.org/2019/03/google-platinum-debconf19.html
[blogpt]: https://bits.debian.org/2019/03/google-platinum-debconf19-pt-BR.html
