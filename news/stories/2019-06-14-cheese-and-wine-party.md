---
title: Cheese and Wine Party!
---

Leia em Português abaixo.

In a month we will be in Curitiba to start [DebCamp][debcamp] and
[DebConf19][debconf19] \o/

This C&W is the 15th official DebConf Cheese and Wine party. The first C&W was
improvised in Helsinki during DebConf 5, in the so-called "French" room. Cheese
and Wine parties are now a tradition for DebConf.

The event is very simple: bring good edible stuff from your country. We like
cheese and wine, but we love the surprising stuff that people bring from all
around the world or regions of Brazil. So, you can bring non-alcoholic drinks
or a typical food that you would like to share as well. Even if you don't
bring anything, feel free to participate: our priorities are our attendants
and free cheese.

We have to organize for a great party. An important part is planning - We want
to know what you are bringing, in order to prepare the labels and organizing
other things.

So, please go to our [wiki page][wikipage] and add what you will bring!

If you don't have time to buy before travel, we list some places where you can
buy cheese and wine in Curitiba. There are more information about C&W, what
you can bring, vegan cheese, Brazil customs regulations and non-alcoholic
drinks at [our site][cheese].

C&W will happen on July 22nd, 2019 (Monday) after 19:30.

We are looking forward to seeing you all here!

*****

# Festa dos queijos e vinhos

Em um mês estaremos em Curitiba para começar a [DebCamp][debcamp]
e a [DebConf19][debconf19] \o/

Esta será a 15a edição do Queijos e Vinhos. O primeiro Queijos e Vinhos foi
improvisado em Helsinki durante a DebConf5 na "Sala Francesa". Agora o Queijos
e Vinhos é uma tradição na DebConf.

O evento é muito simples: traga algo comestível do seu país ou região. Nós
gostamos de queijos e vinhos, mas nós gostamos de coisas surpreendentes que
as pessoas trazem das regiões do mundo ou do Brasil. Então, você pode trazer
também bebidas não alcoólicas ou comidas típicas que você gostaria de
compartilhar. Se você não trouxer nada, sinta-se livre para participar: nossa
prioridade são os participantes e queijos de graça.

Nós precisamos organizar uma grande festa. Uma parte importante é o
planejamento - Nós queremos saber o que você vai trazer, para preparar os
adesivos de identificação e outras coisas.

Então, por favor acesse a nossa [página wiki][wikipage] e adicione o que você vai trazer

Se você não tiver tempo para comprar antes de viajar, nós listamos alguns
lugares onde você pode comprar queijos e vinhos em Curitiba.

Tem mais informações sobre o Queijos e Vinhos, o que você pode trazer,
queijo vegano, regulamentos alfandegários do Brasil e bebidas não
alcoólicas no [nosso site][cheese].

O Queijos e Vinhos será no dia 22 de julho (segunda-feira) após às 19:30.

Estamos ansiosos(as) para ver todos(as) vocês aqui!

[debcamp]: https://wiki.debian.org/DebConf/19/DebCamp
[debconf19]: https://debconf19.debconf.org
[wikipage]: https://wiki.debian.org/DebConf/19/CheeseWine#What_will_be_available_during_the_party.3F
[cheese]: https://debconf19.debconf.org/about/cheese-and-wine-party
