---
title: DebConf19 starts today in Curitiba
---

Leia em Português abaixo.

[DebConf19](https://debconf19.debconf.org/), the 20th annual
Debian Conference, is taking place in Curitiba, Brazil
from from July 21 to 28, 2019.

Debian contributors from all over the world have come together at
[Federal University of Technology - Paraná (UTFPR)](https://debconf19.debconf.org/about/venue/)
in Curitiba, Brazil, to participate and work in a conference exclusively
run by volunteers.

Today the main conference starts with over 350 attendants expected
and 121 activities scheduled,
including 45- and 20-minute talks and team meetings ("BoF"),
workshops, a job fair as well as a variety of other events.

The full schedule at
[https://debconf19.debconf.org/schedule/](https://debconf19.debconf.org/schedule/)
is updated every day, including activities planned ad-hoc
by attendees during the whole conference.

If you want to engage remotely, you can follow the
[**video streaming** available from the DebConf19 website](https://debconf19.debconf.org/)
of the events happening in the three talk rooms:
_Auditório_ (the main auditorium), _Miniauditório_ and _Sala de Videoconferencia_.
Or you can join the conversation about what is happening
in the talk rooms:
  [**#debconf-auditorio**](https://webchat.oftc.net/?channels=#debconf-auditorio),
  [**#debconf-miniauditorio**](https://webchat.oftc.net/?channels=#debconf-miniauditorio) and
  [**#debconf-videoconferencia**](https://webchat.oftc.net/?channels=#debconf-videoconferencia)
(all those channels in the OFTC IRC network).

You can also follow the live coverage of news about DebConf19 on
[https://micronews.debian.org](https://micronews.debian.org) or the @debian
profile in your favorite social network.

DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Anti-Harassment team)
are available to help so both on-site and remote participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the [web page about the Code of Conduct in DebConf19 website](https://debconf19.debconf.org/about/coc/)
for more details on this.


Debian thanks the commitment of numerous [sponsors](https://debconf19.debconf.org/sponsors/)
to support DebConf19, particularly our Platinum Sponsors:
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
and [**Lenovo**](https://www.lenovo.com).



*****

# DebConf19 começa hoje em Curitiba

[DebConf19](https://debconf19.debconf.org), 20º Conferência Anual do Debian,
está sendo realizada em Curitiba, Brasil de 21 a 28 de julho de 2019.

Contribuidores Debian de todo o mundo se reuniram na 
[Universidade Federal de Tecnologia - Paraná (UTFPR)](https://debconf19.debconf.org/about/venue/)
em Curitiba, Brasil, para participar e trabalhar em uma conferência
exclusivamente executada por voluntários.

Hoje a conferência principal começa, são esperados mais de 350 participantes
e estão 121 atividades programadas, incluindo palestras de 45 e 20 minutos
e reuniões de equipe ("BoF"), oficinas, uma feira de empregos,
bem como uma variedade de outros eventos.

A programação completa em esta no endereço
[https://debconf19.debconf.org/schedule/](https://debconf19.debconf.org/schedule/)
é atualizada todos os dias, incluindo atividades planejadas e formadas
pelos participantes durante toda a conferência.

Se você quiser se envolver remotamente, você pode seguir o 
[**streaming de vídeo** disponível no site da DebConf19](https://debconf19.debconf.org/)
dos eventos que acontecem nas três salas de conversação:
_Auditório_ (auditório principal), _Miniauditório_ e _Sala de Videoconferência_.
ou participar das conversas sobre o que está acontecendo nas salas de conversação: 
  [**#debconf-auditorio**](https://webchat.oftc.net/?channels=#debconf-auditorio),
  [**#debconf-miniauditorio**](https://webchat.oftc.net/?channels=#debconf-miniauditorio) e
  [**#debconf-videoconferencia**](https://webchat.oftc.net/?channels=#debconf-videoconferencia)
(todos esses canais na rede OFTC IRC).

Você também pode acompanhar a cobertura ao vivo de notícias sobre DebConf19 em
[https://micronews.debian.org](https://micronews.debian.org) ou o @debian perfil na sua rede social favorita.

A DebConf está comprometida com um ambiente seguro e bem-vindo para todos os participantes.
Durante a conferência, várias equipes (Front Desk, equipe de boas-vindas e equipe anti-assédio)
estão disponíveis para ajudar, para que os participantes no local e remotos tenham
a melhor experiência possível na conferência e encontrar soluções para qualquer problema que possa surgir.
Veja a [página web sobre o Código de Conduta no site da DebConf19](https://debconf19.debconf.org/about/coc/)
para mais detalhes sobre isso.


O Debian agradece aos inúmeros [patrocinadores](https://debconf19.debconf.org/sponsors/)
por seu compromisso com DebConf19, particularmente seus patrocinadores Platinum:
[Infomaniak](https://www.infomaniak.com),
[Google](https://google.com/)
e [Lenovo](https://www.lenovo.com).
